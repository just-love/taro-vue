import Vue from 'vue'
import './app.css'
import footers from './components/footer/footers.vue'
import headers from './components/header/headers.vue'
import swiper from './components/swiper/swiper.vue'
import box from './components/functionBox/functionBox.vue'
import boxFour from './components/fourBox/fourBox.vue'
import focus from './components/focus/focus.vue'
import taroSixBox from './components/sixBox/sixBox.vue'
import schoolSelectionGuide from './components/guide/guide.vue'
import askedQuestions from './components/issue/issue.vue'
import campusOpenHouse  from './components/campus/campus.vue'
import schoolName from './components/schoolName/schoolName.vue'
import IntroduceMore from './components/IntroduceMore/IntroduceMore.vue'

Vue.component("c-footer",footers)
Vue.component("c-header",headers)
Vue.component('swiper-taro',swiper)
Vue.component('functionBox',box)
Vue.component('fourBox',boxFour)
Vue.component('focusBox',focus)
Vue.component('sixBox',taroSixBox)
Vue.component('guide',schoolSelectionGuide)
Vue.component('issue',askedQuestions)
Vue.component('campus',campusOpenHouse)
Vue.component('schoolHeader',schoolName)
Vue.component('IntroduceMore',IntroduceMore)
const App = {
  onShow (options) {
  },
  render(h) {
    // this.$slots.default 是将要会渲染的页面
    return h('block', this.$slots.default)
  }
}

export default App
