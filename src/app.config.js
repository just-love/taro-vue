export default defineAppConfig({
  pages: [
    'pages/index/index',
    'pages/f-content/index', 
    'pages/f-content2/index', 
    'pages/InternationalSchools/index',
    'pages/InternationalSchools1/index',
    'pages/InternationalSchools2/index',
    'pages/InternationalSchools3/index',
    'pages/Information/index',
    'pages/list/index',
    'pages/citySchools/index',
    'pages/schoolDetails/index',
    'pages/special/index',
    'pages/moreGuides/index',
    'pages/moreIssue/index',
    'pages/internationalSchoolPage/index',
    'pages/overseasHighSchoolPage/index',
    'pages/foreignUniversityPage/index',
    'pages/preparatoryCourseForStudyingAbroad/index',
    'pages/schoolDetails2/index',
    'pages/campusOpenDay/index',
    'pages/demo1/index',
    'pages/demo2/index',
    'pages/regionalTopics/index',
    'pages/best-answer/index',
  ],
  window: {
    backgroundTextStyle: 'light',
    navigationBarBackgroundColor: '#fff',
    navigationBarTitleText: 'WeChat',
    navigationBarTextStyle: 'black'
  }
})
